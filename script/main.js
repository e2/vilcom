var orderItemsCount = 0;
var orderSum = 0;

var orderItems = [];


ready = function(){


	$('.card-item').click(function(event) {

		event.preventDefault();
		var itemID = $(this).attr('id');

		if (!$(this).hasClass('checked')) {

			orderItemsCount += 1;
			orderItems.push(itemID);

			var currentSum = $(this).find('.item-price-value').text();
			orderSum += currentSum-0;
			var currentItemName = $(this).find('.item-name').text();

			$('.order-list').append('<div class="order-list-item" id="orders-list-'+itemID+'">'+currentItemName+'</div>');

		} else {


			orderItemsCount -= 1;	
			var currentSum = $(this).find('.item-price-value').text();
			orderSum -= currentSum;

			$('#orders-list-'+itemID).remove();
			
		}

		$(this).toggleClass('checked');
		$('.order-sum-value').text(orderSum);
		

		if (orderItemsCount > 0) {
			$('.order-link').removeClass('active');
			$('.order-info').addClass('active');



		} else {
			$('.order-link').addClass('active');
			$('.order-info').removeClass('active');

		}
	});


	$('.scroll-link').click(function(e) {
		e.preventDefault();

		var linkOffset = -40;
		if ($($.attr(this, 'href')).data('scroll-link-offset')) {
			linkOffset += $($.attr(this, 'href')).data('scroll-link-offset')
		};

		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top + linkOffset
			
		}, 500);
	});

	$('.promoblock').click(function(event) {
		window.location.href = $(this).find('a').attr('href');
	});
	$('.promoblock').hover(function() {
		$(this).find('a').addClass('hovered');
	}, function() {
		$(this).find('a').removeClass('hovered');
	});


	
};


$(document).ready(ready);













